
/**
 * WSConsultarTablasGDESkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WSConsultarTablasGDESkeleton java skeleton for the axisService
     */
    public class WSConsultarTablasGDESkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WSConsultarTablasGDELocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param nombreTabla
                                     * @param nombreDataset
                                     * @param nombresCamposConsulta
                                     * @param valoresCamposConsulta
                                     * @param nombreCamposResultado
         */
        

                 public co.net.une.www.gis.WSConsultarTablasGDERSType consultarTablasGDE
                  (
                  java.lang.String nombreTabla,java.lang.String nombreDataset,java.lang.String nombresCamposConsulta,java.lang.String valoresCamposConsulta,java.lang.String nombreCamposResultado
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("nombreTabla",nombreTabla);params.put("nombreDataset",nombreDataset);params.put("nombresCamposConsulta",nombresCamposConsulta);params.put("valoresCamposConsulta",valoresCamposConsulta);params.put("nombreCamposResultado",nombreCamposResultado);
		try{
		
			return (co.net.une.www.gis.WSConsultarTablasGDERSType)
			this.makeStructuredRequest(serviceName, "consultarTablasGDE", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    